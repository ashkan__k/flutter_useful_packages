import 'dart:io';
import 'package:http/http.dart' as http;
import 'package:dio/dio.dart';
import 'package:file_picker/file_picker.dart';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';

class FilePickerPage extends StatefulWidget {
  const FilePickerPage({Key? key}) : super(key: key);

  @override
  State<FilePickerPage> createState() => _FilePickerPageState();
}

class _FilePickerPageState extends State<FilePickerPage> {
  final dio = Dio();

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: Column(
        mainAxisAlignment: MainAxisAlignment.center,
        children: [
          Center(
            child: Column(
              children: [
                ElevatedButton(
                  onPressed: () async {
                    FilePickerResult? result = await FilePicker.platform.pickFiles(
                      type: FileType.custom,
                      allowedExtensions: ['jpg', 'pdf', 'doc'],
                    );

                    if (result != null) {
                      File file = File(result.files.single.path!);

                      // UploadViaDio(file);
                      UploadViaHttp(file);
                    } else {
                      print('canceled');
                      // User canceled the picker
                    }
                  },
                  child: Icon(Icons.file_upload),
                )
              ],
            ),
          ),
        ],
      ),
    );
  }

  Future<void> UploadViaDio(File file) async {
    String fileName = file.path.split('/').last;

    // Upload Product
    final formData = FormData.fromMap({
      'title': 'shayan',
      'text': 'shayan@gmail.com',
      'is_active': 0,
      'image': await MultipartFile.fromFile(
        file.path,
        filename: fileName,
        // contentType: new MediaT("image", "jpeg")
      ),
    });

    print('ssssssssssssssssss');
    print(file.path.toString());
    print(fileName);

    try {
      final response = await dio.post(
        'http://192.168.251.40:8000/products/',
        data: formData,
        options: Options(
          followRedirects: false,
          // will not throw errors
          validateStatus: (status) => true,
        ),
      );

      print('resuuuuuuuuuuuuuult:');
      print(response.statusCode);
      print(response.data);
    } on DioError catch (e) {
      print(e.message);
    }
  }

  Future<void> UploadViaHttp(File file) async {
    String fileName = file.path.split('/').last;

    Map<String, String> headers = {
      'Content-Type': 'multipart/form-data',
    };

    Map<String, String> body = {
      'title': 'dfdjshfjkdsf',
      'text': 'dfdjshfjkdsf',
      'is_active': '0',
    };

    var request = http.MultipartRequest(
        'POST', Uri.parse('http://192.168.251.40:8000/products/'))
      ..fields.addAll(body)
      ..headers.addAll(headers)
      ..files.add(await http.MultipartFile.fromPath('image', file.path));

    print('wwwwwwwwwwwwwwwwwwwwww');
    print(fileName);

    http.StreamedResponse streamedResponse = await request.send();
    final response = await http.Response.fromStream(streamedResponse);

    print(response.statusCode);
    print(response.body);

    // var request = http.MultipartRequest(
    //     'POST', Uri.parse('http://192.168.251.40:8000/products/'));
    // request.fields['title'] = 'dfjdsklfjds';
    // request.fields['text'] = 'dfjdsklfjds';
    // request.fields['is_active'] = '1';
    //
    // request.files.add(http.MultipartFile(
    //     'image', file.readAsBytes().asStream(), file.lengthSync(),
    //     filename: fileName));
    // http.StreamedResponse streamedResponse = await request.send();
    // final response = await http.Response.fromStream(streamedResponse);
    //
    // print(response.statusCode);
    // print(response.body);
  }
}
